const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const user = require("../db/models/user");

router.use(express.json());

router.get("/getAllUser", async(req, res) => {
    try {
        const getAllUsers = await user.findAll();
        console.log(getAllUsers);
        res.status(200).json({
          status: "success",
          data: getAllUsers,
        });
      } catch (err) {
        console.error(err.message);
      }
})

module.exports = router;