const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const { Sequelize } = require("sequelize");
const room = require("../db/models/room");

router.use(express.json());

router.post("/createRoom", async (req, res) => {
    try {
        console.log(req.body.roomid);
        const new_room = await room.create({
            room_id: req.body.roomid
        });
        res.status(201).json({
            status: "success",
        })
    } catch (error) {
        console.log(error);
    }
})

router.post("/findRoom", async (req, res) => {
    try {
        const getRoom = await room.count({
            where: {
                room_id: req.body.roomid
            }
        });
        console.log(getRoom);
        if (getRoom == 1) {
            res.status(200).json({
                status: "success",
                data: getRoom,
            });
        } else {
            res.status(200).json({
                status: "failure"
            });
        }
    } catch (err) {
        console.error(err.message);
    }
})

router.post("/getAllMessages", async (req, res) => {
    try {
        const allMessages = await room.findOne({
            where: {
                room_id: req.body.roomid
            }
        })
        res.status(200).json({
            status: "success",
            data: allMessages
        })
    } catch (err) {
        console.error(err.message);
    }
})

router.post("/newMessage", async (req, res) => {
    try {
        room.update(
            {
                messages: Sequelize.fn(
                    "array_append",
                    Sequelize.col("messages"),
                    req.body.message
                ),
            },
            {
                where: { room_id: req.body.roomid },
            }
        );
        res.status(200).json({
            status: "success",
            data: {
                message: req.body.message,
                roomid: req.body.roomid,
            },
        });
    } catch (error) {

    }
})

router.post("/currentlyPlaying", async (req, res) => {
    try {
        room.update(
            {
                current_video: req.body.video
            },
            {
                where: { room_id: req.body.roomid },
            }
        );
        res.status(200).json({
            status: "success",
            data: {
                video: req.body.video,
                roomid: req.body.roomid,
            },
        });
    } catch (error) {

    }
})

router.post("/updateTime", async (req, res) => {
    try {
        room.update(
            {
                time: req.body.time,
                current_video: req.body.video,
            },
            {
                where: { room_id: req.body.roomid },
            }
        );
        res.status(200).json({
            status: "success",
            data: {
                time: req.body.time,
                roomid: req.body.roomid,
            },
        });
    } catch (error) {

    }
})

router.post("/getRoomInfo", async (req, res) => {
    try {
        const roomInfo = await room.findOne({
            where: {
                room_id: req.body.roomid
            }
        })
        res.status(200).json({
            status: "success",
            data: roomInfo
        })
    } catch (err) {
        console.error(err.message);
    }
})


module.exports = router;