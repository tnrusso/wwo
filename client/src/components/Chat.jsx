import React, { useEffect, useContext } from "react";
import { Chatbox } from "./Chatbox";
import RoomAPI from "../apis/RoomAPI";
import io from "socket.io-client";
import './styles/Chat.css'


const ENDPOINT = "https://watchwithothers.herokuapp.com/";
let socket = io(ENDPOINT);

export function Chat(props) {
    const [text, setText] = React.useState('');

    // console.log(props.room);
    // console.log(props.messages);

    const handleSubmit = async (e) => {
        e.preventDefault();
        if (text != "" && props.name.trim() != "") {
            try {
                let new_message = props.name + ": " + text;
                // console.log(new_message)
                setText('');
                const response = await RoomAPI.post("/newMessage", {
                    roomid: props.room,
                    message: new_message
                })
                socket.emit("new-message", {
                    room: props.room,
                    message: new_message
                });
            } catch (error) {
                console.log(error);
            }
        }
    }

    function handleChange(e) {
        e.preventDefault();
        setText(e.target.value);
        if (e.key === 'Enter') {
            handleSubmit();
        }
    }

    return (
        <div id="chat-div">
            <div id='chatbox'>
                <Chatbox room={props.room} msg={props.messages} />
            </div>
            <div>
                <form onSubmit={handleSubmit}>
                    <input type="text" value={text} onChange={handleChange} maxLength="1000" className="text-box" />
                </form>
            </div>
        </div>

    );
}