import React, { useEffect, useContext } from "react";
import './styles/Chat.css'


export function Chatbox(prop) {

    return (
        <div className="chat">
            <ul className="chatRoom">
                {prop.msg.map((message, index) => (
                    <li key={index} className="chatMessages">
                        <p>
                            {message}
                        </p>
                    </li>
                ))}
            </ul>
        </div>
    );
}