import React, { useEffect, useContext } from "react";
import './styles/Loading.css'

export function Loading() {

    return (
        <div className="loader">
            Please Wait - Creating Room...
            <div className="circle">

            </div>
        </div>
    );
}