import React, { useEffect, useContext, useState } from "react";
import ReactPlayer from 'react-player/youtube'
import io from "socket.io-client";
import { Chat } from '../components/Chat';
import { useParams, useNavigate } from 'react-router-dom'
import { Button, Form, Nav, Container, Row, Col, Modal } from "react-bootstrap";
import RoomAPI from "../apis/RoomAPI";
import './styles/vidpage.css';

const ENDPOINT = "https://watchwithothers.herokuapp.com/";
let socket = io(ENDPOINT);

export function VideoPage() {
    const [status, setStatus] = useState(1);
    const [name, setName] = useState("");
    const [nameSet, setIsNameSet] = useState(false);
    const [vol, setVolume] = useState(1);
    const [videoURLInput, setVideoURLInput] = useState("");
    const [video, setVideo] = useState("");
    const [play, setPlay] = useState(true);
    const [played, setPlayed] = useState(0);
    const [messages, setMessages] = React.useState([]);
    const [showLeave, setShowLeave] = useState(false);
    const [loaded, setLoaded] = useState(0);
    const [time, setTime] = useState(0);

    const { id } = useParams()

    let navigate = useNavigate();

    const handleClose = () => setShowLeave(false);
    const handleShow = () => setShowLeave(true);


    // Connecting to existing room
    useEffect(() => {
        async function onStart() {
            if (id.length != 36 && status) {
                setStatus(0);
                return;
            }
            try {
                const response = await RoomAPI.post("/findRoom", {
                    roomid: id
                })
                if (response.data.status == "failure") {
                    setStatus(0);
                } else {
                    socket.emit("join-room", {
                        room: id
                    })
                }
            } catch (e) {
                console.error(e);
            }
        }
        onStart();
    }, [])

    // Chat Messages
    async function refreshMessages() {
        try {
            const response = await RoomAPI.post("/getAllMessages", {
                roomid: id
            })
            setMessages(response.data.data.messages);
            setTimeout(async function () {
                const res = await RoomAPI.post("/getAllMessages", {
                    roomid: id
                })
                setMessages(res.data.data.messages);
            }, 150);
        } catch (e) {
            console.error(e);
        }
    }

    useEffect(() => {
        socket.on("get-all-messages", (d) => {
            refreshMessages();
        });
    }, []);

    useEffect(() => {
        refreshMessages();
    }, []);

    // Video
    function handleSubmit(e) {
        e.preventDefault();
        setVideo(videoURLInput)
        socket.emit("change-vid", {
            room: id,
            video: videoURLInput
        })
        setVideoURLInput('');
    }

    function handleVideoChange(e) {
        setVideoURLInput(e.target.value);
        if (e.key === 'Enter') {
            handleSubmit();
        }
    }

    useEffect(() => {
        socket.on("vid-changes-from-server", (d) => {
            setPlay(d.data.play);
        });
    }, []);

    useEffect(() => {
        socket.on("set-new-vid", (d) => {
            setVideo(d.data.video)
            const saveVid = async function () {
                try {
                    const response = await RoomAPI.post("/currentlyPlaying", {
                        roomid: id,
                        video: d.data.video
                    })
                } catch (e) { }
            }
            saveVid();
        });
    }, []);

    async function vidTimer(props) {
        if (parseInt(props) > 0 && loaded == 1) {
            const response = await RoomAPI.post("/updateTime", {
                roomid: id,
                video: video,
                time: parseInt(props)
            })
        }
    }

    function vidChanges(props) {
        socket.emit("vid-changes-from-client", {
            room: id,
            play: props
        });
    }

    const copyLink = async copyMe => {
        try {
            await navigator.clipboard.writeText(window.location.href);
        } catch (err) {

        }
    };

    // Video Settings
    function changeVolume(props) {
        setVolume(parseFloat(props));
    }

    // Name
    function handleNameChange(e) {
        setName(e.target.value);
    }

    async function confirmName() {
        let set = false;
        if (name.trim() != "" && name.length > 0) {
            setIsNameSet(true);
            set = true;
        }
        if (set === true) {
            const response = await RoomAPI.post("/getRoomInfo", {
                roomid: id
            })
            setTime(response.data.data.current_video);
            if (response.data.data.current_video + "&t=" + response.data.data.time != "&t=") {
                setVideo(response.data.data.current_video + "&t=" + response.data.data.time)
            }
            setTimeout(async function () {
                setVideo(response.data.data.current_video + "&t=" + response.data.data.time)
                setLoaded(1);
            }, 200);
        }
    }

    function leaveRoom() {
        navigate("/");
    }


    return (
        <div id="home">
            {status ?
                <>
                    <Modal
                        show={!nameSet}
                        animation={true}
                        backdrop="static"
                        keyboard={false}
                        centered
                    >
                        <Modal.Header>
                            <Modal.Title>
                                Chat Appearance
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Form>
                                <Form.Group className="mb-3">
                                    <Form.Label>How You Want To Appear In The Chat?</Form.Label>
                                    <Form.Control type="text" placeholder="Enter name" value={name} onChange={handleNameChange} />
                                    <Form.Text className="text-muted">

                                    </Form.Text>
                                </Form.Group>
                            </Form>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={confirmName} variant="primary">Confirm</Button>
                        </Modal.Footer>
                    </Modal>
                    <Container>
                        <Row>
                            <Nav justify id="nav-bar">
                                <Nav.Item>
                                    <h1>WWO</h1>
                                </Nav.Item>
                            </Nav>
                        </Row>
                        <Row className="row1">
                            <Col xs={12} lg={12} md={12} sm={12} xl={6} className="col">
                                <div className="outer-div">
                                    <div className="react-player block">
                                        <ReactPlayer
                                            width='100%'
                                            height='100%'
                                            url={video}
                                            volume={vol}
                                            playing={play}
                                            onPlay={() => vidChanges(true)}
                                            onPause={() => vidChanges(false)}
                                            onProgress={(progress) => {
                                                vidTimer(progress.playedSeconds);
                                            }}
                                        />
                                    </div>
                                    <span className="settings">
                                        <label htmlFor="vol">Volume</label>
                                        <input type="range" id="vol" name="points" min="0" max="1" step="0.01" value={vol} onChange={(e) => changeVolume(e.target.value)}></input>
                                    </span>
                                </div>

                            </Col>
                            <Col xs={12} lg={12} md={12} sm={12} xl={6} className="col">
                                <div className="outer-div2">
                                    <div className="block">
                                        <Chat room={id} name={name} messages={messages} />
                                    </div>
                                    {nameSet ?
                                        <span className="settings">
                                            <p className="signed-in-as">
                                                Signed in as "{name}"
                                            </p> </span> :
                                        <>
                                            <span className="settings">
                                                <p className="signed-in-as">
                                                    <br></br>
                                                </p></span>
                                        </>}
                                </div>
                            </Col>
                        </Row>
                        <Row id='bottom-row'>
                            <Col xs={12} lg={4} md={4} sm={4} xl={4} className="bottom-col">
                                <span className="share-url">
                                    Share Link
                                    <Button variant="light" onClick={copyLink}>Copy</Button>
                                </span>
                            </Col>
                            <Col xs={12} lg={4} md={4} sm={4} xl={4} className="bottom-col">
                                <form onSubmit={handleSubmit} className="video-input">
                                    <input className="video-input" type="text" placeholder="Enter Video URL" value={videoURLInput} onChange={handleVideoChange} maxLength="1000" />
                                </form>

                            </Col>
                            <Col xs={12} lg={4} md={4} sm={4} xl={4} className="bottom-col">
                                <Button className="leave-btn" variant="danger" onClick={handleShow}>Leave Room</Button>
                            </Col>
                        </Row>
                    </Container>
                    <Modal
                        show={showLeave}
                        onHide={handleClose}
                        animation={true}
                        keyboard={false}
                        centered
                    >
                        <Modal.Header>
                            <Modal.Title>
                                Leave Room
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            Are you sure you want to leave this room?
                            <br></br><br></br>
                            You will be redirected to the home page.
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={handleClose} variant="primary">Stay</Button>
                            <Button onClick={leaveRoom} variant="danger">Leave</Button>
                        </Modal.Footer>
                    </Modal>
                </>
                :
                <>
                    <h2>404 Not Found</h2>
                </>
            }
        </div >
    )
}