import React, { useState } from 'react';
import { Button, Modal } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { v4 as uuidv4 } from 'uuid';
import RoomAPI from '../apis/RoomAPI';
import { Loading } from '../components/Loading';
import logo from '../images/logo-tp.png'
import youtube from '../images/youtube.png'
import copylink from '../images/copylink.png'
import shareurl from '../images/shareurl.png'
import watchfinal from '../images/watchfinal.png'
import Collapsible from 'react-collapsible';

export function Home() {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  let navigate = useNavigate();

  const createRoom = async () => {
    let room = uuidv4();
    handleShow();
    // create room in db
    try {
      const response = await RoomAPI.post("createRoom", {
        roomid: room
      })
    } catch (error) {
      console.log(error);
    }
    setTimeout(async function () {
      navigate("/room/" + room);
    }, 500);
  }

  const login = () => {
    navigate("/login")
  }

  const signup = () => {
    navigate("/signup")
  }

  return (
    <>
      <Modal
        show={show}
        onHide={handleClose}
        animation={false}
        backdrop="static"
        keyboard={false}
        centered
      >
        <Loading />
      </Modal>
      <div id="home">
        <div id="nav-bar">
          <h1>WWO</h1>
        </div>
        <div className='content-container'>
          <div className='ul-container'>
            <h1>
              Watch W/ Others{' '}
              <img className='youtube' src={youtube}></img>
            </h1>
            <hr></hr>
            <br></br>
            <h2>How To Use (Click to expand)</h2>
            <br></br>
            <ul className='home-ul'>
              <li>
                <Collapsible trigger="+ Create a room">
                  <div className='button-container'>
                    <br></br>
                    <button className='custom-btn' onClick={createRoom}><b>Create Room</b></button>
                  </div>
                  <hr></hr>
                </Collapsible>
              </li>
              <br></br>
              <li>
                <Collapsible trigger="+ Share the room URL for others to join">
                  <br></br>
                  <img className='instructions-image' src={copylink}></img>
                  <hr></hr>
                </Collapsible>
              </li>
              <br></br>
              <li>
                <Collapsible trigger="+ Enter the YouTube video URL that you want to play">
                  <br></br>
                  <img className='instructions-image' src={shareurl}></img>
                  <hr></hr>
                </Collapsible>
              </li>
              <br></br>
              <li>
                <Collapsible trigger="+  Watch the YouTube video in real time with your friends!">
                  <br></br>
                  <img className='instructions-image' src={watchfinal}></img>
                  <hr></hr>
                </Collapsible>
              </li>
            </ul>
          </div>
          <br></br>
        </div>
      </div>
    </>
  )
}