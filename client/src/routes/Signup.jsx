import React, { useEffect, useContext, useState } from "react";
import { useParams, useNavigate } from 'react-router-dom'
import './styles/home.css';

export function Signup() {
    const { id } = useParams()

    let navigate = useNavigate();

    return (
        <div className='page'>
            <div className='content'>
                <div id='signup-login-box'>
                    <form>
                        <h1>Sign Up</h1>
                        <p>Please fill in this form to create an account.</p>
                        <hr></hr>
                        <br></br>
                        <label for="user">Username:</label>
                        <br></br>
                        <input type="text" id="user" />
                        <br></br><br></br>
                        <label for="pass">Password:</label>
                        <br></br>
                        <input type="password" id="pass" />
                        <br></br>
                        <br></br>
                        <button className="custom-btn">Sign Up</button>
                    </form>
                    <br></br>
                    <br></br>
                    Already have an account? <a href='/login'>Login here</a>
                </div>
            </div>
        </div>
    )
}