import React, { useEffect, useContext, useState } from "react";
import { useParams, useNavigate } from 'react-router-dom'
import './styles/home.css';

export function Login() {
    const { id } = useParams()

    let navigate = useNavigate();

    return (
        <div className='page'>
            <div className='content'>
                <div id='signup-login-box'>
                    <form>
                        <h1>Login</h1>
                        <p>Please fill in this form to sign in to your account.</p>
                        <hr></hr>
                        <br></br>
                        <label for="user">Username:</label>
                        <br></br>
                        <input type="text" id="user" />
                        <br></br><br></br>
                        <label for="pass">Password:</label>
                        <br></br>
                        <input type="password" id="pass" />
                        <br></br>
                        <br></br>
                        <button className="custom-btn">{' '}Login{' '}</button>
                    </form>
                    <br></br>
                    <br></br>
                    Don't have an account? <a href='/signup'>Sign up here</a>
                </div>
            </div>
        </div>
    )
}