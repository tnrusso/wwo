const express = require('express');
const http = require("http");
const cors = require("cors");
const bodyParser = require("body-parser");
const path = require("path");
var cookieParser = require('cookie-parser');
var session = require('express-session')

const app = express();
const server = http.createServer(app);
const port = process.env.PORT || 8080;

var corsOptions = {
  origin: "http://localhost:3000"
};

app.use(cors(corsOptions));
if (process.env.NODE_ENV === "production") {
  // server static content
  // npm run build
  app.use(express.static(path.join(__dirname, "client/build")));
}

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));


app.use(cookieParser());
app.use(session({
  secret: '1234567890', // just a long random string
  resave: false,
  saveUninitialized: true
}));

var io;

if (process.env.NODE_ENV == "production") {
  io = require("socket.io")(server, {
    cors: {
      origin: "https://watchwithothers.herokuapp.com/",
      methods: ["GET", "POST"],
      allowedHeaders: ["my-custom-header"],
      credentials: true,
    },
  });
} else {
  io = require("socket.io")(server, {
    cors: {
      origin: "http://localhost:3000",
      methods: ["GET", "POST"],
      allowedHeaders: ["my-custom-header"],
      credentials: true,
    },
  });
}


io.on("connection", (socket) => {
  // socket.join("room1")

  console.log('socket connected')
  socket.emit('test', {
    data: 'test'
  })

  socket.on("join-room", (data) => {
    socket.join(data.room);
  })

  socket.on("vid-changes-from-client", (data) => {
    console.log(data);
    socket.to(data.room).emit('vid-changes-from-server', {
      data: data
    })
  })

  socket.on("change-vid", (data) => {
    console.log(data);
    socket.to(data.room).emit('set-new-vid', {
      data: data
    })
  })

  socket.on("new-message", (data) => {
    socket.to(data.room).emit('get-all-messages', {
      data: data
    })
  })

  socket.on("disconnect", () => {
    console.log("A disconnection has been made");
  });
});

app.post("/post", (req, res) => {
  res.redirect("/");
});


//routes
app.use("/api/v1/room", require("./routes/room"));

//db
const db = require("./db/index");

db.authenticate()
  .then(() => console.log("Database connected..."))
  .catch((err) => console.log(err));

// Sync DBs
db.sync()
  .then(() => console.log("Models have been synced..."))
  .catch((err) => console.log(err));

app.get("/*", (req, res) => {
  const url = path.join(__dirname, "client/build", "index.html");
  res.sendFile(url);
});

server.listen(port, () => console.log(`Listening on port ${port}`));

