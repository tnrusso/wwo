require("dotenv").config();

// module.exports = {
//   HOST: "localhost",
//   USER: "postgres",
//   PASSWORD: "pass123",
//   DB: "mydb",
//   dialect: "postgres",
//   pool: {
//     max: 5,
//     min: 0,
//     acquire: 30000,
//     idle: 10000
//   }
// };

module.exports = {
  development: {
    username: "postgres",
    password: "pass123",
    database: "mydb",
    host: "localhost",
    dialect: "postgres",
  },
  production: {
    username: "root",
    password: null,
    database: "database_production",
    host: "127.0.0.1",
    dialect: "postgres",
  },
};