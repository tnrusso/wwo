const { UUID } = require("sequelize");
const { STRING } = require("sequelize");
const { Sequelize, DataTypes } = require("sequelize");
const db = require("../index");

const room = db.define(
    "room",
    {
        room_id: {
            type: DataTypes.UUID,
            defaultValue: Sequelize.UUIDV4,
            allowNull: false,
            primaryKey: true,
        },
        current_video: {
            type: DataTypes.STRING,
            defaultValue: ""
        },
        time: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        messages: {
            type: DataTypes.ARRAY(DataTypes.STRING),
            defaultValue: [],
        }
    }
);

module.exports = room;